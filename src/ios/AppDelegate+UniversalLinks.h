//
//  AppDelegate+UniversalLinks.m
//  Sukker.no
//
//  Created by Knut Eirik Leira Hjelle on 05/11/2018.
//

#import "AppDelegate.h"

@interface AppDelegate (UniversalLinks)

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler;

@property(nonatomic, retain) NSUserActivity *launchUserActivity;

@end
