//
//  AppDelegate+UniversalLinks.m
//  Sukker.no
//
//  Created by Knut Eirik Leira Hjelle on 05/11/2018.
//

#import <objc/runtime.h>
#import "AppDelegate+UniversalLinks.h"
#import "UniversalLinks.h"

static NSString *const PLUGIN_NAME = @"UniversalLinks";
static char launchUserActivityKey;

@implementation AppDelegate (UniversalLinks)

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    BOOL result = [super application:application didFinishLaunchingWithOptions:launchOptions];

    if(launchOptions[UIApplicationLaunchOptionsUserActivityDictionaryKey] != nil)
    {
        NSDictionary *activities = launchOptions[UIApplicationLaunchOptionsUserActivityDictionaryKey];
        if(activities != nil)
        {
            NSUserActivity *activity = activities[@"UIApplicationLaunchOptionsUserActivityKey"];
            if(activity != nil)
            {
                self.launchUserActivity = activity;
            }
        }
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openUrlFromActivity:)
                                                 name:CDVPageDidLoadNotification
                                               object:nil];

    return result;
}

- (void)openUrlFromActivity:(id)handleOpenUrl {
    if(!self.launchUserActivity)
    {
        return;
    }
    UniversalLinks *plugin = [self.viewController getCommandInstance:PLUGIN_NAME];
    if (plugin == nil) {
        return;
    }

    [plugin handleUserActivity:self.launchUserActivity];
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler {
    if (![userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb] || userActivity.webpageURL == nil) {
        return NO;
    }

    UniversalLinks *plugin = [self.viewController getCommandInstance:PLUGIN_NAME];
    if (plugin == nil) {
        return NO;
    }

    return [plugin handleUserActivity:userActivity];
}

- (NSUserActivity *)launchUserActivity
{
    return objc_getAssociatedObject(self, &launchUserActivityKey);
}

- (void)setLaunchUserActivity:(NSUserActivity *)launchUserActivity
{
    objc_setAssociatedObject(self, &launchUserActivityKey, launchUserActivity, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
