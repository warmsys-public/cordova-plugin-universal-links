//
//  UniversalLinks.m
//  Sukker.no
//
//  Created by Knut Eirik Leira Hjelle on 05/11/2018.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

/**
 *  Plugin main class.
 */
@interface UniversalLinks : CDVPlugin

- (BOOL)handleUserActivity:(NSUserActivity *)userActivity;

- (BOOL)openUrl:(NSString *)url;
@end