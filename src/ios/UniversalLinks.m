//
//  UniversalLinks.m
//  Sukker.no
//
//  Created by Knut Eirik Leira Hjelle on 05/11/2018.
//

#import "UniversalLinks.h"

@implementation UniversalLinks

#pragma mark Public API

- (BOOL)handleUserActivity:(NSUserActivity *)userActivity {
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb])
    {
        NSURL *url = [userActivity webpageURL];
        [self openUrl:url.absoluteString];
        return YES;
    }
    return NO;
}

- (void)openUrl:(NSString *)url {
    NSString *jsCommand = [NSString stringWithFormat:@"window.handleOpenURL(\"%@\");", url];
    [self.commandDelegate evalJs:jsCommand];
}
@end
